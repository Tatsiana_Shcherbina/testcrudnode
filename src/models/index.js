const sqlite3 = require('sqlite3').verbose(),
    config=require('../config/db.config');



const constructor = new sqlite3.Database(config.source, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message)
        throw err
    }else{
        console.log('Connected to the SQLite database.')
    }
});

const database={};

database.connect = constructor;

require('../loaders/data.loader')(database.connect);

module.exports = database;
