const DBSOURCE = 'db.sqlite',
    table_user='user',
    columns_user=[
        'id INTEGER PRIMARY KEY AUTOINCREMENT',
        'name text',
        'email text UNIQUE',
        'password text'],
    constraint_user='CONSTRAINT email_unique UNIQUE (email)'


module.exports={
    source:DBSOURCE,
    table_user,
    columns_user,
    constraint_user
}
