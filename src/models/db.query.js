const user=require('./user.model.js');

let CREATE_USER=`CREATE TABLE ${user.table_name} (
            id INTEGER PRIMARY KEY AUTOINCREMENT, 
            ${user.name} text, 
            ${user.email} text UNIQUE, 
            ${user.pass} text, 
            CONSTRAINT email_unique UNIQUE (${user.email}))`,
    INSERT_USER=`INSERT INTO ${user.table_name} (${user.name}, ${user.email}, ${user.pass}) VALUES (?,?,?)`,
    SELECT_ALL='select * from ',
    DELETE_USER=`DELETE FROM ${user.table_name} WHERE id = ?`,
    UPDATE_USER=`UPDATE ${user.table_name} set ${user.name} = COALESCE(?,${user.name}),${user.email} = COALESCE(?,${user.email}), ${user.pass} = COALESCE(?,${user.pass}) WHERE id = ?`

function createTable(tableName, columns, constraint) {
    let query = `CREATE TABLE ${tableName} (`;
    for(let column in columns) {
        query += `${column},`
    }
    query+=`${constraint});`
    return query;
}

function selectAll(tablename){
    return SELECT_ALL+`${tablename};`
}

function selectOne(tablename, selectparam='id'){
    return `select * from ${tablename} where ${selectparam} = ?`
}


module.exports={
    CREATE_USER,
    INSERT_USER,
    createTable,
    selectAll,
    selectOne,
    UPDATE_USER,
    DELETE_USER
}
