module.exports = db => {
    const md5 = require('md5'),
        query = require('../models/db.query'),
        config = require('../config/db.config'),

        create = () => {
        db.run(query.CREATE_USER,
            (err) => {
                if (err) {
                    // Table already created
                } else {
                    // Table just created, creating some rows
                    db.run(query.INSERT_USER, ["admin", "admin@example.com", md5("admin123456")])
                    db.run(query.INSERT_USER, ["user", "user@example.com", md5("user123456")])
                }

                console.log(query.createTable(config.table_user, config.columns_user, config.constraint_user))
            });
    }
}
